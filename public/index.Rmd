---
pagetitle: "Clase 09: Bagging, Random Forests, Boosting"
title: "**Big Data and Machine Learning en el Mercado Inmobiliario**"
subtitle: "**Clase 09:** Bagging, Random Forests, Boosting"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Universidad de los Andes | [BD-ML](https://ignaciomsarmiento.github.io/teaching/BDML)
output: 
  html_document:
    theme: flatly
    highlight: haddock
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(knitr,tidyverse,fontawesome,kableExtra)
# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!---------------------->
## **[0.] Configuración inicial**

Para replicar las secciones de esta clase, primero debe descargar el siguiente [proyecto de R](https://gitlab.com/lectures-r/big-data-real-state-202202/clase-09/-/archive/main/clase-09-main.zip?path=clase-09) y abrir el archivo `clase-09.Rproj`. 

### **0.0 Instalar/llamar las librerías de la clase**

```{r , eval=T}
require(pacman) 
p_load(tidyverse,rio,janitor,skimr,
       caret,ranger,  ## trees/ramdon-forest
       h2o, ## nucleos y metodo de estimación
       rattle) ## plot trees
```

### **0.1 leer datos**
```{r , eval=T}
db <- import("input/data_regresiones.rds")
```

### **0.2 prepare db**
```{r , eval=T}
### tibble to matrix
df <- model.matrix(~.+ I(bathrooms^2) + I(surface_total^2) + I(dist_cbd^2) -1 -price , db) %>% 
      as_tibble() %>% clean_names()

df$price <- db$price/1000000 %>% unlist()
```

<!---------------------->
## **[1.] Recap**

### **1.1 Arboles de regresión**

#### **Ventajas**

* Sencillo, intuitivo y fácil de interpretar.

* Captura muy bien las no linealidades de los datos.

#### **Desventajas**

* Overfitting: Tienen un sesgo bajo (se ajustan bastante bien al set de entrenamiento) pero una varianza alta (al momento de predecir el error es alto). 

* Son muy sensibles a los datos de entrenamiento, es decir ante ligeras variaciones en los datos de entrenamiento se puede generar un arbol totalmente diferente.

```{r , eval=T}
## Fijar semilla
set.seed(3430)
sample <- sample(x = nrow(df) , nrow(df)*0.8)
train <- df[sample,]
test <- df[-sample,]

## Caret ya hace una selección del mejor hiperparámetro complexity parameter (cp): 
## cuando se le especifica el método rpart sin necesidad de incluir alguna grilla.
## cp=minimize{SSE + b|T|}
## complexity parameter (b)
## number of terminal nodes of the tree (T)
tree_1 <- train(price ~ .,
                data = train, 
                method = "rpart", ## regression/clasification tree 
                trControl = trainControl(number=5 , method="repeatedcv"))
tree_1

plot(tree_1)

## plot tree
fancyRpartPlot(tree_1$finalModel)

## rmse in sample
rmse_in = min(tree_1$results$RMSE)
rmse_in

## rmse outsample
test$price_tree1 <- predict(object=tree_1 , newdata=test) %>% unlist()
rmse_out <- sqrt(with(test,mean((price-price_tree1)^2)))
rmse_out

## arbol 2
set.seed(2104)
sample <- sample(x = nrow(df) , nrow(df)*0.8)
train <- df[sample,]
test <- df[-sample,]

tree_2 <- train(price ~ .,
                data = train, 
                method = "rpart",
                trControl = trainControl(number=5 , method="repeatedcv"))

fancyRpartPlot(tree_2$finalModel)

min(tree_2$results$RMSE)
test$price_tree2 <- predict(object=tree_2 , newdata=test) %>% unlist()
sqrt(with(test,mean((price-price_tree2)^2)))
```

<!---------------------->
## **[2.] Bagging y Random Forests**

```{r , eval=T}
### 2.1 Solucion: Random Forests preserva bajo sesgo y reducen varianza
# En lugar de entrenar un arbol, se entrenan varios arboles.
# Pero no es suficiente, porque si se sigue entrenando los arboles usando el mismo
# set de datos, se sigue teniendo el problema de varianza alta.
# Entonces cada arbol se entrena con un subset de datos aleatorio diferente.
# Y se obtiene un promedio de todos los arboles.

### 2.2 Algoritmo de Random Forests con Bagging or Boostrap Agregation
## 1. Fijar el numero de arboles
## 2. Crear el subset de entrenamiento de cada arbol
###   Bootstrapping: Muestreo aleatorio con reemplazo
## 3. Seleccionar el numero de predictores
###   Se eligen aleatoriamente m predictores ; m = p^1/2 
###   donde p = numero de predictores en el dataset
###   Nota: Mencionar la importancia de la aleatoriedad en la eleccion de los predictores
### 4. Obtener la predicci

## modelo 1
rf_1 <- train(price ~ .,
              data = train[1:1000,], 
              method = "ranger", ## ramdon forest
              trControl = trainControl(number=5 , method="repeatedcv"))
rf_1

## plot
plot(rf_1)
```

```{r , eval=F}
### 2.3 Tunear un bosque
## 1. Fijar numero de predictores y criterio de parada y elegir numero de arboles
## 2. Fijar numero de arboles y criterio de parada y elegir el numero de predictores
## 3. Fijar numero de predictores y numero de arboles y elegir el criterio de parada

## Creamos una grilla para tunear el random forest
tunegrid_rf <- expand.grid(mtry = c(2,4,8), 
                           min.node.size = c(2,5,10),
                           splitrule = "variance")

## ranger me permite optimizar mas hiperparametros
## fit model
rf_2 <- train(price ~ .,
              data = train[1:1000,], 
              method = "ranger", 
              tuneGrid = tunegrid_rf)
rf_2

# plot
plot(rf_2)
```

```{r , eval=T}
## Ahora vamos a ver como cambian los resultados cuando cambio el número de árboles
resultados <- tibble()
for (t in seq(1, 100, 10)) {
     modelo_i <- ranger(price ~ ., 
                        data = train, 
                        num.trees = t,
                        mtry = 8, 
                        min.node.size = 10,
                        splitrule = "variance")
     
     rmse_in = sqrt(modelo_i[["prediction.error"]])
     test$price_rf_i <- predict(modelo_i , data=test)$predictions
     rmse_out <- sqrt(with(test,mean((price-price_rf_i)^2)))
     
     result_i <- tibble(arboles = t,rmse_in = rmse_in , rmse_out = rmse_out)
     resultados <- rbind(resultados, result_i)
}
resultados

ggplot() +
geom_line(data=resultados , aes(x=arboles , y=rmse_out) , col="red") + 
geom_line(data=resultados , aes(x=arboles , y=rmse_in) , col="blue") + 
geom_point() + theme_bw() +
labs(x = "Número de árboles", y = "RMSE",
     title = "Variación en el desempeño del Random Forest para diferente número de árboles")
```

<!---------------------->
## **[3.] Boosting**

```{r , eval=F}
## Diferencia con Ramdon Forest?
### Ramdon forest los arboles son independientes, en Boosting aprenden

### Algoritmo de boosting
## 1. Set f(x)=0 y r_i=y_i para todo i en el conjunto de entrenamiento.
## 2. Para b=1,2,...,B, repita:
### 2.1 Ajuste un arbol f^b con d particiones (d+1 nodos terminales) para el train-data (X,r)
### 2.2 Actualice f agregando una versión reducida del nuevo árbol:
###     f(x) <- f(x) + Lambda*f^b(x) 
### 2.3 Actualice los residuos
###     r_i <- r_i + Lambda*f^b(x_i)
## 3. Output del modelo potenciado
###   f(x) = suma { Lambda*f^b(x) } ; para b=1,2,...,B.

## Creamos una grilla para tunear el gbm
tunegrid_gbm <- expand.grid(learn_rate = c(0.1, 0.01, 0.001),
                            ntrees = 50,
                            max_depth = 2, ## 
                            col_sample_rate = 1,
                            min_rows = 70) 

## le voy a poner 6 nucleos porque 8
h2o.init(nthreads = 6)

## modelo 3
boos_1 <- train(price ~ .,
                data = train, 
                method = "gbm_h2o", 
                trControl = trainControl(number=5 , method="repeatedcv"),
                tuneGrid = tunegrid_gbm) 

# plot
boos_1
plot(modelo3)

```

